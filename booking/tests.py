import json

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from django.db.models import Max

from .services import *
from .models import *

class ServicesTests(TestCase):
    def setUp(self):
        self.configure_request()
        self.configure_objects()

    def configure_request(self):
        self.request_factory = RequestFactory()
        self.request_user = User.objects.create_user(
            username = "sample", email = "sample@sample.co.uk", password = "petersburg")
    
    def configure_objects(self):
        providerData = {"email": "acme@acme.org", "location": "Auckland"}
        providerA = Provider(name = "A", **providerData)
        providerB = Provider(name = "B", **providerData)
        providerC = Provider(name = "C", **providerData)
        providerA.save()
        providerB.save()
        providerC.save()
        
        bookA = Book(authors = "J. May", name = "Book A", year = 2016, provider = providerA)
        bookB = Book(authors = "J. Hammond", name = "Book B", year = 2014, provider = providerB)
        bookC = Book(authors = "J. Clarkson", name = "Book C", year = 2015, provider = providerC)
        bookA.save()
        bookB.save()
        bookC.save()

        orderA = Order(year = 2016, state = 1, user = self.request_user, provider = providerA)
        orderB = Order(year = 2015, state = 2, user = self.request_user, provider = providerB)
        orderC = Order(year = 2014, state = 3, user = self.request_user, provider = providerC)
        orderA.save()
        orderB.save()
        orderC.save()

        orderedBookB = OrderedBook(book = bookB, order = orderB)
        orderedBookB.save()

class OrdersServicesTests(ServicesTests):
    def exec(self, **args):
        request = self.configure_method_invoke(args)
        response = OrdersServices(request).exec()
        return json.loads(response.content)

    def configure_method_invoke(self, args):
        request_url = "/orders/services/?{}".format(
            "&".join("{}={}".format(key, args[key]) for key in args))
        request = self.request_factory.get(request_url)
        request.user = self.request_user
        return request

class OrdersServicesSelectMethodTests(OrdersServicesTests):
    def test_select_orders(self):
        response = self.exec(method = "select")
        self.assertEqual(len(response["objects"]), 3)
    
    def test_select_orders_with_state_filter(self):
        response = self.exec(method = "select", state = 1)
        self.assertEqual(len(response["objects"]), 1)
    
    def test_select_orders_with_state_zero_filter(self):
        response = self.exec(method = "select", state = 0)
        self.assertEqual(len(response["objects"]), 3)
    
    def test_select_orders_with_year_filter(self):
        response = self.exec(method = "select", year = 2016)
        self.assertEqual(len(response["objects"]), 1)
    
    def test_select_order_with_id(self):
        response = self.exec(method = "select", order_id = 1)
        self.assertEqual(response["objects"]["id"], 1)
    
    def test_select_order_with_wrong_id(self):
        response = self.exec(method = "select", order_id = 0)
        self.assertEqual(response["success"], False)

class OrdersServicesRemoveMethodTests(OrdersServicesTests):
    def test_remove_with_correct_id(self):
        order_id = 1
        response = self.exec(method = "remove", order_id = order_id)
        self.assertRaises(Order.DoesNotExist, Order.objects.get, id = order_id)

    def test_remove_with_wrong_id(self):
        order_id = 0
        response = self.exec(method = "remove", order_id = order_id)
        self.assertEqual(response["success"], False)

class OrdersServicesAppendMethodTests(OrdersServicesTests):
    def test_append(self):
        book_id = order_id = 1
        response = self.exec(method = "append", book_id = book_id, order_id = order_id)
        self.assertTrue(OrderedBook.objects.get(book_id = book_id, order_id = order_id))
    
    def test_double_append(self):
        book_id = order_id = 1
        response = self.exec(method = "append", book_id = book_id, order_id = order_id)
        firstAppend = response["success"]
        response = self.exec(method = "append", book_id = book_id, order_id = order_id)
        secondAppend = response["success"]
        self.assertTrue(firstAppend and not secondAppend)
    
    def test_append_with_not_same_providers(self):
        book_id = 1
        order_id = 2
        response = self.exec(method = "append", book_id = book_id, order_id = order_id)
        self.assertFalse(response["success"])

class OrdersServicesCreateMethodTests(OrdersServicesTests):
    def test_create_with_correct_provider_id(self):
        provider_id = 1
        order_id = Order.objects.all().aggregate(Max("id"))
        order_id = order_id["id__max"]
        response = self.exec(method = "create", provider_id = provider_id)
        self.assertTrue(response["success"] and Order.objects.get(id = order_id + 1))
    
    def test_create_with_wrong_provider_id(self):
        provider_id = 0
        before_order_id = Order.objects.all().aggregate(Max("id"))
        before_order_id = before_order_id["id__max"]
        response = self.exec(method = "create", provider_id = provider_id)
        after_order_id = Order.objects.all().aggregate(Max("id"))
        after_order_id = after_order_id["id__max"]
        self.assertTrue(not response["success"] and (before_order_id == after_order_id))

class OrdersRemoveOrderedBookMethodTests(OrdersServicesTests):
    def test_remove_ordered_book(self):
        ordered_book_id = 1
        response = self.exec(method = "remove_ordered_book", ordered_book_id = ordered_book_id)
        self.assertRaises(OrderedBook.DoesNotExist, OrderedBook.objects.get, id = ordered_book_id)
    
    def test_remove_ordered_book_with_wrong_id(self):
        ordered_book_id = 0
        response = self.exec(method = "remove_ordered_book", ordered_book_id = ordered_book_id)
        self.assertFalse(response["success"])

class OrdersUpdateMetadataMethodTests(OrdersServicesTests):
    def test_update_with_wrond_order_id(self):
        order_id = 0
        response = self.exec(method = "update_meta_data", order_id = order_id)
        self.assertFalse(response["success"])

    def test_update_without_args(self):
        order_id = 1
        response = self.exec(method = "update_meta_data", order_id = order_id)
        self.assertTrue(response["success"])
    
    def test_update_year(self):
        order_id = 1
        order_new_year = 1995
        response = self.exec(method = "update_meta_data", order_id = order_id, year = order_new_year)
        order = Order.objects.get(pk = order_id)
        self.assertTrue(response["success"] and order.year == order_new_year)
    
    def test_update_state(self):
        order_id = 1
        order_new_state = 3
        response = self.exec(method = "update_meta_data", order_id = order_id, state = order_new_state)
        order = Order.objects.get(pk = order_id)
        self.assertTrue(response["success"] and order.state == order_new_state)

class OrdersUpdateOrderedBookDataMethodTests(OrdersServicesTests):
    def test_update_with_wrond_ordered_book_id(self):
        ordered_book_id = 0
        response = self.exec(method = "update_ordered_book_data", ordered_book_id = ordered_book_id)
        self.assertFalse(response["success"])
    
    def test_update_without_args(self):
        ordered_book_id = 1
        response = self.exec(method = "update_ordered_book_data", ordered_book_id = ordered_book_id)
        self.assertTrue(response["success"])

    def test_update_price(self):
        ordered_book_id = 1
        new_price = 150
        response = self.exec(method = "update_ordered_book_data", ordered_book_id = ordered_book_id, price = new_price)
        ordered_book = OrderedBook.objects.get(pk = ordered_book_id)
        self.assertTrue(response["success"] and ordered_book.price == new_price)
    
    def test_update_amount(self):
        ordered_book_id = 1
        new_amount = 2
        response = self.exec(method = "update_ordered_book_data", ordered_book_id = ordered_book_id, amount = new_amount)
        ordered_book = OrderedBook.objects.get(pk = ordered_book_id)
        self.assertTrue(response["success"] and ordered_book.amount == new_amount)
