from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from django.db import models

from django.shortcuts import get_object_or_404
from django.shortcuts import get_list_or_404

from json import dumps
from openpyxl import Workbook

from .models import ORDER_STATES
from .models import OrderedBook
from .models import Provider
from .models import Catalog
from .models import Order
from .models import Book

from .providers import MashinCatalogManager

class Request:
    def __init__(self, request):
        self._user = request.user
        self._args = request.GET if len(request.GET) else request.POST

    def user(self):
        return self._user

    def method(self):
        return self.arg("method")

    def arg(self, arg):
        return self._args.get(arg, "")

class Services:
    def __init__(self, request):
        self._request = Request(request)

    def exec(self):
        method_handler = self.build_method_handler(self._request.method())
        return method_handler.exec(self._request)

class ServiceMethod:
    def exec(self, request):
        result = self.serialize_result(
            self.perform_request(request)
        )
        return self.make_response(result)

class BooksServices(Services):
    def build_method_handler(self, method):
        return {
            "select": BooksSelectMethod,
            "create": BooksCreateMethod
        }.get(method, EmptyMethod)()

class EmptyMethod(ServiceMethod):
    def perform_request(self, request):
        pass

    def serialize_result(self, result):
        pass

    def make_response(self, result):
        return HttpResponseBadRequest()

class JsonResponseMethod(ServiceMethod):
    def perform_request(self, request):
        result = dict.fromkeys((
            "message", "success", "objects"))
        try:
            result["success"] = True
            result["objects"] = self.recv_objects(request)
        except Exception as e:
            result["message"] = str(e)
            result["success"] = False
        return result

    def serialize_result(self, result):
        return dumps(result, ensure_ascii=False).encode("UTF-8")

    def make_response(self, result):
        return HttpResponse(result)

class BooksSelectMethod(JsonResponseMethod):
    def recv_objects(self, request):
        books = Book.objects.all()
        books = BooksProviderFilter(BooksAuthorsFilter(
            BooksNameFilter(BooksYearFilter())
        )).filter(books, request)
        return [book.serialize() for book in books]

class BooksCreateMethod(JsonResponseMethod):
    def recv_objects(self, request):
        book_fields = (
            "year",
            "pages",
            "price",
            "publication",
            "volume-number",
            "isbn",
            "name",
            "authors",
            "volume-name"
        )
        book_provider = Provider.objects.get(pk = request.arg("provider"))
        book_fields = {field.replace("-", "_"): request.arg(field) for field in book_fields}
        Book(provider = book_provider, **book_fields).save()

class SelectFilter:
    def __init__(self, next_filter = None):
        self._filter = next_filter

    def filter(self, objects, request):
        objects = self.apply_filter(objects, request)
        continue_filtering = objects and self._filter
        return self._filter.filter(objects, request) if continue_filtering else objects

class BooksProviderFilter(SelectFilter):
    def apply_filter(self, books, request):
        provider_id = request.arg("provider")
        provider_id = int(provider_id) if provider_id else 0
        return books.filter(provider__id = provider_id) if provider_id > 0 else books

class BooksAuthorsFilter(SelectFilter):
    def apply_filter(self, books, request):
        authors = request.arg("authors")
        return books.filter(authors__contains = authors) if authors else books

class BooksNameFilter(SelectFilter):
    def apply_filter(self, books, request):
        name = request.arg("name")
        return books.filter(name__contains = name) if name else books

class BooksYearFilter(SelectFilter):
    def apply_filter(self, books, request):
        year = request.arg("year")
        return books.filter(year = int(year)) if year else books

class OrdersServices(Services):
    def build_method_handler(self, method):
        return {
            "select": OrdersSelectMethod,
            "remove": OrdersRemoveMethod,
            "append": OrdersAppendMethod,
            "create": OrdersCreateMethod,
            "remove_ordered_book": OrdersRemoveOrderedBook,
            "update_meta_data": OrdersUpdateMetaData,
            "update_ordered_book_data": OrdersUpdateOrderedBookData,
            "export": OrdersCsvExport,
        }.get(method, EmptyMethod)()

class OrdersSelectMethod(JsonResponseMethod):
    def recv_objects(self, request):
        result = None
        order_id = request.arg("order_id")
        if order_id:
            result = Order.objects.get(pk = int(order_id))
            result = result.serialize()
        else:
            result = Order.objects.filter(user = request.user())
            result = OrdersProviderFilter(
                OrdersStateFilter(OrdersYearFilter()
            )).filter(result, request)
            result = [order.serialize() for order in result]
        return result

class OrdersCsvExport(ServiceMethod):
    def perform_request(self, request):
        user = request.user()
        order_id = int(request.arg("order_id"))
        order = Order.objects.get(user = user, pk = order_id)
        response = HttpResponse(content_type = "application/vnd.ms-excel")
        response["Content-Disposition"] = "attachment;filename=order-{}-{}-{}.xlsx".format(
            order.id, order.year, user.username)
        
        excel_doc = Workbook()
        excel_doc_page = excel_doc.active
        excel_doc_page.title = "Заказ"
        
        for row in (
            ("Издательство", order.provider.name),
            ("Комплектатор", "{} {}".format(
                user.last_name,
                user.first_name
            )),
            ("Год формирования", order.year),
            ("Статус", ORDER_STATES[order.state - 1])
        ):
            excel_doc_page.append(row)

        excel_doc_page.append([
            "Номер позиции",
            "Название",
            "Авторы",
            "Объем (стр.)",
            "ISBN",
            "Издательство",
            "Город",
            "Год",
            "Том",
            "Название издания",
            "Номер издания",
            "Цена (пост.)",
            "Цена (собс.)",
            "Кол - во позиций",
            "Стоимость"
        ])

        order_total_price = order_total_items = 0
        for pos, ordered_book in enumerate(OrderedBook.objects.filter(order = order), 1):
            book_total_price = ordered_book.amount * ordered_book.price
            order_total_items += ordered_book.amount
            order_total_price += book_total_price
            excel_doc_page.append([
                pos,
                ordered_book.book.name,
                ordered_book.book.authors,
                ordered_book.book.pages,
                ordered_book.book.isbn,
                ordered_book.book.provider.name,
                ordered_book.book.provider.location,
                ordered_book.book.year,
                ordered_book.book.publication,
                ordered_book.book.volume_name,
                ordered_book.book.volume_number,
                ordered_book.book.price,
                ordered_book.price,
                ordered_book.amount,
                book_total_price
            ])
        
        for row in (
            ("Всего позиций", order_total_items),
            ("Общая стоимость заказа", order_total_price),
        ):
            excel_doc_page.append(row)
        
        excel_doc.save(response)
        return response

    def serialize_result(self, result):
        return result

    def make_response(self, result):
        return result

class OrdersRemoveOrderedBook(JsonResponseMethod):
    def recv_objects(self, request):
        ordered_book_id = int(request.arg("ordered_book_id"))
        OrderedBook.objects.get(pk = ordered_book_id).delete()

class OrdersUpdateMetaData(JsonResponseMethod):
    def recv_objects(self, request):
        order_id = int(request.arg("order_id"))
        order = Order.objects.get(pk = order_id)
        order_year = request.arg("year")
        if order_year: order.year = int(order_year)
        order_state = request.arg("state")
        if order_state: order.state = int(order_state)
        order.save()

class OrdersUpdateOrderedBookData(JsonResponseMethod):
    def recv_objects(self, request):
        ordered_book_id = int(request.arg("ordered_book_id"))
        ordered_book = OrderedBook.objects.get(pk = ordered_book_id)
        book_price = request.arg("price")
        if book_price: ordered_book.price = int(book_price)
        book_amount = request.arg("amount")
        if book_amount: ordered_book.amount = int(book_amount)
        ordered_book.save()

class OrdersRemoveMethod(JsonResponseMethod):
    def recv_objects(self, request):
        order_id = int(request.arg("order_id"))
        order = Order.objects.get(pk = order_id)
        order.delete()

class OrdersAppendMethod(JsonResponseMethod):
    def recv_objects(self, request):
        order_id = int(request.arg("order_id"))
        book_id = int(request.arg("book_id"))
        order = Order.objects.get(pk = order_id)
        book = Book.objects.get(pk = book_id)
        if (order.provider.id != book.provider.id):
            raise Exception("Providers of book and order must be same")
        if OrderedBook.objects.filter(order = order, book = book):
            raise Exception("Book is already ordered")
        OrderedBook(order = order, book = book, price = book.price).save()

class OrdersCreateMethod(JsonResponseMethod):
    def recv_objects(self, request):
        provider_id = int(request.arg("provider_id"))
        provider = Provider.objects.get(pk = provider_id)
        order = Order(user = request.user(), provider = provider, state = 1)
        order.save()

class OrdersProviderFilter(SelectFilter):
    def apply_filter(self, orders, request):
        provider_id = request.arg("provider_id")
        provider_id = int(provider_id) if provider_id else 0
        return orders.filter(provider__id = provider_id) if provider_id else orders

class OrdersStateFilter(SelectFilter):
    def apply_filter(self, orders, request):
        state = request.arg("state")
        state = int(state) if state else 0
        return orders.filter(state = state) if state else orders

class OrdersYearFilter(SelectFilter):
    def apply_filter(self, orders, request):
        year = request.arg("year")
        return orders.filter(year = int(year)) if year else orders

class CatalogsServices(Services):
    def build_method_handler(self, method):
        return {
            "info": CatalogsInfoMethod,
            "refresh": CatalogsRefreshMethod
        }.get(method, EmptyMethod)()

class CatalogsInfoMethod(JsonResponseMethod):
    def recv_objects(self, request):
        return [catalog.serialize() for catalog in Catalog.objects.all()]

class CatalogsRefreshMethod(JsonResponseMethod):
    def recv_objects(self, request):
        catalog_id = request.arg("catalog_id")
        if catalog_id: catalog_id = int(catalog_id)
        else: raise Exception("Catalog ID is required")
        catalog = Catalog.objects.get(id = catalog_id)
        self.build_catalog_updater(catalog).refresh()
    
    def build_catalog_updater(self, catalog):
        manager = None
        provider_name = catalog.provider.name
        if provider_name == "Машиностроение":
            manager = MashinCatalogManager()
        else:
            raise Exception("Selected catalog provider currently isn't supported")
        return CatalogUpdater(catalog, manager)

class CatalogUpdater:
    def __init__(self, catalog, manager):
        self._catalog = catalog
        self._manager = manager
        
    def refresh(self):
        for catalog_item in self.determine_new_items():
            catalog_item = self._manager.complete_item(catalog_item)
            catalog_item.book.save()
        self._catalog.save()
        
    def determine_new_items(self):
        db_items = Book.objects.filter(provider = self._catalog.provider)
        db_items = set(db_items)
        catalog_items = self._manager.receive_items()
        catalog_items = set(catalog_items)
        return catalog_items - db_items
