from django.contrib import admin
from .models import *

admin.site.register(Book)
admin.site.register(Order)
admin.site.register(Catalog)
admin.site.register(Provider)
admin.site.register(OrderedBook)
