from datetime import datetime

from django.db import models
from django.contrib.auth.models import User

ORDER_STATES = (
    "В обработке",
    "Сформирован",
    "Подготовлен к экспорту в АБИС"
)

class SerializedModel:
    def serialize(self):
        return serialize(self)

def serialize(object):
    serialized = dict()
    for attr in object.__serialize__():
        value = getattr(object, attr)
        serialized[attr] = serialize(value) if hasattr(value, "__serialize__") else value
    return serialized

class Provider(models.Model, SerializedModel):
    name = models.CharField(max_length=64)
    email = models.CharField(max_length=64)
    location = models.CharField(max_length=64)
    is_publisher = models.BooleanField(default=True)

    def __serialize__(self):
        return ("id", "name", "email", "location", "is_publisher")

    def __str__(self):
        return str(self.serialize())

class Catalog(models.Model, SerializedModel):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    refresh_date = models.DateTimeField(auto_now=True)
    
    def __serialize__(self):
        return ("id", "provider")
    
    def serialize(self):
        catalog = super().serialize()
        catalog["refresh_date"] = datetime.strftime(self.refresh_date, "%d.%m.%Y %H:%M:%S")
        catalog["items"] = Book.objects.filter(provider = self.provider).count()
        return catalog
    
    def __str__(self):
        return str(self.serialize())

class Book(models.Model, SerializedModel):
    year = models.PositiveSmallIntegerField(default=0)
    isbn = models.CharField(default="", max_length=64)
    pages = models.PositiveSmallIntegerField(default=0)
    price = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(default="", max_length=128)
    authors = models.CharField(default="", max_length=128)
    publication = models.PositiveSmallIntegerField(default=0)
    volume_name = models.CharField(default="", max_length=64)
    volume_number = models.PositiveSmallIntegerField(default=0)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)

    def __serialize__(self):
        return ("id", "year", "pages", "price", "isbn", "name", "authors", 
                "publication", "volume_name", "volume_number", "provider")
    
    def __hash__(self):
        return hash(self.name)
    
    def __eq__(self, o):
        return hash(self) == hash(o)

    def __str__(self):
        return str(self.serialize())

class Order(models.Model, SerializedModel):
    year = models.PositiveSmallIntegerField(default=0)
    state = models.PositiveSmallIntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)

    def __serialize__(self):
        return ("id", "year", "state", "provider")

    def serialize(self):
        order_price = 0
        order_items = 0
        order_books = []
        order = super().serialize()
        for ordered_book in OrderedBook.objects.filter(order = self):
            order_books.append(ordered_book.serialize())
            order_price += ordered_book.price * ordered_book.amount
            order_items += ordered_book.amount
        order["items"] = order_items
        order["books"] = order_books
        order["price"] = order_price
        return order

    def __str__(self):
        return str(self.serialize())

class OrderedBook(models.Model, SerializedModel):
    price = models.PositiveSmallIntegerField(default=0)
    amount = models.PositiveSmallIntegerField(default=0)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __serialize__(self):
        return ("id", "book", "order", "amount", "price")

    def __str__(self):
        return str(self.serialize())
