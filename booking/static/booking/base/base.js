function invokeGetApi(apiUrl, method, args, callback) {
    invokeApi(jQuery.get, apiUrl, method, args, callback);
}

function invokePostApi(apiUrl, method, args, callback) {
    invokeApi(jQuery.post, apiUrl, method, args, callback);
}

function invokeApi(request, apiUrl, method, args, callback) {
    request(apiUrl + "?method=" + method, args, function(resp) {
        callback((resp) ? JSON.parse(resp) : undefined);
    });
}
