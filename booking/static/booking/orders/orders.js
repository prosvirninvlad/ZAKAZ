$(function() {
    receiveOrdersItems({}, ordersTableBuilder);
});

function clearOrdersTable() {
    clearTable("orders-table");
}

function ordersTableBuilder(response) {
    clearOrdersTable();
    if (response.success) {
        $("#orders-table").find("tbody").append(response.objects.reduce(function(markup, order) {
            var orderData = createOrderTableColumnMarkup(order);
            var orderActions = createActionsTableColumnMarkup([
                {"type": "info", "img": "refresh", "func": sprintf("customizeOrder(%d)", order.id), "title": "Изменить"},
                {"type": "warning", "img": "download", "func": sprintf("exportOrderCsv(%d)", order.id), "title": "Экспорт в Excel"},
                {"type": "danger", "img": "remove", "func": sprintf("removeOrder(%d)", order.id), "title": "Удалить"},
            ]);
            return sprintf("%s<tr>%s%s</tr>", markup, orderData, orderActions);
        }, ""));
    } else {
        alert(sprintf("Error: %s.", response.message));
    }
}

function exportOrderCsv(orderId) {
    location.href = sprintf("%s?method=export&order_id=%d", ordersServicesUrl, orderId);
}

function removeOrder(orderId) {
	var responseHandler = function(response) {
		if (response.success) {
			applyOrdersFilter();
		} else {
			alert(sprintf("Ошибка: %s.", response.message));
		}
	};
	if (confirm("Вы действительно желаете удалить данный заказ?")) {
		invokeOrdersServices("remove", {"order_id": orderId}, responseHandler);
	}
}

var currentOrderId = undefined;
function customizeOrder(orderId) {
    currentOrderId = orderId;
    prepareOrderCustomizeDialog(orderId);
    $("#order-customize-dialog").modal("show");
}

function prepareOrderCustomizeDialog(orderId) {
    invokeOrdersServices("select", {"order_id": orderId}, function(response) {
        if (response.success) {
            var order = response.objects;
            $("#customize-order-year").val(order.year).change();
            $("#customize-order-state").val(order.state).change();
            $("#customize-order-provider").val(order.provider.id).change();
            orderedBooksTableBuilder(order.books);
        } else {
            alert(sprintf("Error: %s.", response.message));
        }
    });
}

function orderedBooksTableBuilder(orderedBooks) {
    clearTable("customize-order-books-table");
    $("#customize-order-books-table").find("tbody").append(orderedBooks.reduce(function(markup, book) {
        var bookData = createBookTableColumnMarkup(book.book);
        var bookChangedHandler = sprintf("changeOrderedBookData(%d)", book.id);
        var bookAmount = sprintf("<td><input type=\"number\" id=\"ordered-book-amount-%d\" class=\"form-control\" value=\"%d\" onchange=\"%s\" /></td>", book.id, book.amount, bookChangedHandler);
        var bookProviderPrice = sprintf("<td><input type=\"number\" id=\"ordered-book-price-%d\" class=\"form-control\" value=\"%d\" onchange=\"%s\" /></td>", book.id, book.price, bookChangedHandler);
        var bookActions = createActionsTableColumnMarkup([
            {"type": "danger", "img": "remove", "func": sprintf("removeCurrentOrderBook(%d)", book.id), "title": "Удалить"},
        ]);
        return sprintf("%s<tr>%s%s%s%s</tr>", markup, bookData, bookProviderPrice, bookAmount, bookActions);
    }, ""));
}

function changeOrderedBookData(orderedBookId) {
    var orderedBookAmount = $(sprintf("#ordered-book-amount-%d", orderedBookId)).val();
    var orderedBookPrice = $(sprintf("#ordered-book-price-%d", orderedBookId)).val();
    invokeOrdersServices("update_ordered_book_data", {
        "price": orderedBookPrice,
        "amount": orderedBookAmount,
        "ordered_book_id": orderedBookId
    }, function(response) {
        if (!response.success)
            alert(sprintf("Error: %s", response.message));
    });
}

function changeCurrentOrderState() {
    var orderStateNode = $("#customize-order-state");
    var orderStateNodeParent = orderStateNode.parent();
    var orderState = orderStateNode.val();
    invokeOrdersServices("update_meta_data", {"order_id": currentOrderId, "state": orderState}, function(response) {
        orderStateNodeParent.toggleClass("has-warning", !response.success);
    });
}

function changeCurrentOrderYear() {
    var orderYearNode = $("#customize-order-year");
    var orderYearNodeParent = orderYearNode.parent();
    var orderYear = orderYearNode.val();
    var isValidOrderYear = orderYear && !isNaN(orderYear);
    if (orderYear && !isNaN(orderYear)) {
        invokeOrdersServices("update_meta_data", {"order_id": currentOrderId, "year": orderYear}, function(response) {
            orderYearNodeParent.toggleClass("has-warning", !response.success);
        });
    }
    orderYearNodeParent.toggleClass("has-danger", !isValidOrderYear);
}

function applyOrdersFilter() {
    receiveOrdersItems(collectOrdersFilterData(), function(response) {
        appendPopover("#apply-orders-filter", {
            "content": sprintf("Найдено элементов: %s", response.objects.length),
            "placement": "left",
            "timeout": 1500
        });
        ordersTableBuilder(response);
    });
}

function removeCurrentOrderBook(orderedBookId) {
    if (confirm("Вы действительно желаете удалить данную позицию? Данная операция необратима.")) {
        invokeOrdersServices("remove_ordered_book", {
            "ordered_book_id": orderedBookId,
        }, function(response) {
            if (response.success) {
                prepareOrderCustomizeDialog(currentOrderId);
            } else {
                alert(sprintf("Error: %s.", response.message));
            }
        });
    }
}

function collectOrdersFilterData() {
    return {
        "provider_id": $("#filter-provider").val(),
        "state": $("#filter-state").val(),
        "year": $("#filter-year").val()
    };
}

function resetOrdersFilter() {
    ["filter-provider", "filter-state"].forEach(function(elem) {
        $("#" + elem).val("0").change();
    });
    $("#filter-year").val("");
    applyOrdersFilter();
}

function createEmptyOrder() {
    var providerId = $("#new-order-provider").val();
    invokeOrdersServices("create", {"provider_id": providerId}, function(response) {
        if (response.success) {
            $("#add-new-order").popover("dispose");
            resetOrdersFilter();
        } else {
            alert(sprintf("Error: %s.", response.message));
        }
    });
}

function hideCreateOrderPopup() {
    $("#add-new-order").popover("dispose");
}
