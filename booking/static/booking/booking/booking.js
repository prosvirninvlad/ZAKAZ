function invokeBooksServices(method, args, callback) {
    invokeGetApi(booksServicesUrl, method, args, callback);
}

function invokeBooksPostServices(method, args, callback) {
    invokePostApi(booksServicesUrl, method, args, callback);
}

function invokeOrdersServices(method, args, callback) {
    invokeGetApi(ordersServicesUrl, method, args, callback);
}

function invokeCatalogsServices(method, args, callback) {
    invokeGetApi(catalogsServicesUrl, method, args, callback);
}

function clearTable(tableId) {
    $("#" + tableId).find("tbody").find("tr").remove();
}

function createOrderTableColumnMarkup(order) {
    var orderData = sprintf("<td class=\"order-data\">%s</td>", [
        {"key": "provider", "value": order.provider.name},
        {"key": "date", "value": sprintf("Год: %d", order.year)},
        {"key": "state", "value": sprintf("Статус: %s", orderStates[order.state])},
    ].reduce(function(result, param) {
        return result + sprintf("<div class=\"order-%(key)s\">%(value)s</div>", param);
    }, ""));
    return [
        {"key": "items", "value": sprintf("%d шт.", order.items)},
        {"key": "price", "value": sprintf("%d руб.", order.price)},
    ].reduce(function(result, param) {
        return result + sprintf("<td class=\"order-%(key)s\">%(value)s</td>", param);
    }, orderData);
}

function createBookTableColumnMarkup(book) {
    var bookDivs = sprintf("<td class=\"book-data\">%s</td>", [
        {"key": "authors", "value": book.authors},
        {"key": "name", "value": book.name},
        {"key": "meta", "value": sprintf("%s. -%s, %d. -%d с.", book.provider.name, book.provider.location, book.year, book.pages)},
        {"key": "isbn", "value": sprintf("ISBN: %s", book.isbn)}
    ].reduce(function(result, param) {
        return result + sprintf("<div class=\"book-%(key)s\">%(value)s</div>", param);
    }, ""));
    return sprintf("%s<td><span>%d руб.</span></td>", bookDivs, book.price);
}

function createActionsTableColumnMarkup(actions) {
    return sprintf(
        "<td class=\"items-actions\"><div class=\"btn-group btn-group-sm\" role=\"group\">%s</div></td>",
        actions.reduce(function(result, param) {
            var markup = "<button type=\"button\" class=\"btn btn-outline-%(type)s\" data-show=\"tip\" " +
                "onclick=\"%(func)s\" data-placement=\"top\" title=\"%(title)s\">" +
                "<img src=\"" + staticUrl + "booking/img/%(img)s.svg" + "\" width=\"30\" height=\"30\"/></button>";
            return result + sprintf(markup, param);
    }, ""));
}

function appendPopover(elementId, options) {
    $(elementId).popover({
        "html": true,
        "title": options.title,
        "content": options.content,
        "placement": options.placement,
    }).popover("show").on("shown.bs.popover", function() {
        setTimeout(function() {$(elementId).popover("dispose")}, options.timeout);
    });
}

function receiveBooksItems(filtersData, tableBuiler) {
    invokeBooksServices("select", filtersData, tableBuiler);
}

function receiveOrdersItems(filterData, tableBuilder) {
    invokeOrdersServices("select", filterData, tableBuilder);
}
