$(function() {
    receiveBooksItems({}, booksTableBuilder);
    receiveOrdersItems({}, ordersTableBuilder);
});

var currentOrderId = 0;

function openOrdersPage() {
    location.href = orderUrl;
}

function clearBooksTable() {
    clearTable("books-table");
}

function booksTableBuilder(response) {
    clearBooksTable();
    if (response.success) {
        $("#books-table").find("tbody").append(response.objects.reduce(function(markup, book) {
            var bookData = createBookTableColumnMarkup(book);
            var bookActions = createActionsTableColumnMarkup([
                {"type": "success", "img": "append", "func": sprintf("appendToTheOrder(%d)", book.id), "title": "Добавить в заказ"},
                {"type": "info disabled", "img": "lookup", "func": "", "title": "Найти похожее"},
            ]);
            return sprintf("%s<tr>%s%s</tr>", markup, bookData, bookActions);
        }, ""));
    } else {
        alert(sprintf("Error: %s.", response.message));
    }
}

function ordersTableBuilder(response) {
    if (response.success) {
        $("#orders-table").find("tbody").append(response.objects.reduce(function(markup, order) {
            var orderData = createOrderTableColumnMarkup(order);
            var orderActions = createActionsTableColumnMarkup([
                {"type": "success", "img": "check", "func": sprintf("setOrderActive(%d)", order.id), "title": "Сделать активным"},
            ]);
            return sprintf("%s<tr>%s%s</tr>", markup, orderData, orderActions);
        }, ""));
    } else {
        alert(sprintf("Error: %s.", response.message));
    }
}

function setOrderActive(order_id) {
    if (confirm("Сделать данный заказ активным?")) {
        currentOrderId = order_id;
        $(".modal").modal("hide");
    }
}

function applyBooksFilter() {
    receiveBooksItems(collectBooksFilterData(), function(response) {
        appendPopover("#apply-books-filter", {
            "content": sprintf("Найдено элементов: %d", response.objects.length),
            "placement": "left",
            "timeout": 1500
        });
        booksTableBuilder(response);
    });
}

function collectBooksFilterData() {
    return {
        "provider": $("#filter-provider").val(),
        "authors": $("#filter-authors").val(),
        "name": $("#filter-name").val(),
        "year": $("#filter-year").val()
    };
}

function resetBooksFilter() {
    ["filter-authors", "filter-name", "filter-year"].forEach(function(elem) {
        $("#" + elem).val("");
    });
    $("#filter-provider").val("0").change();
    applyBooksFilter();
}

function appendToTheOrder(book_id) {
    if (currentOrderId) {
        invokeOrdersServices("append", {"order_id": currentOrderId, "book_id": book_id}, function(response) {
            if (response.success) {
                alert("Книга успешно добавлена в заказ.");
            } else {
                alert(sprintf("Ошибка: %s.", response.message));
            }
        });
    } else {
        alert("Вы не выбрали активный заказ.");
    }
}

function createNewBook() {
    if (checkBookCreatorDialogFields()) {
        invokeBooksServices("create", buildBookObject(), function(response) {
            if (!response.success) {
                alert(sprintf("Error: %s.", response.message));
            } else {
                receiveBooksItems({}, booksTableBuilder);
                $(".modal").modal("hide");
            }
        });
    }
}

function buildBookObject() {
    var book = {};
    var bookFieldPrefix = "#new-book-";
    [
        "year",
        "pages",
        "price",
        "publication",
        "volume-number",
        "isbn",
        "name",
        "authors",
        "volume-name",
        "provider",
    ].forEach(function(fieldName) {
        book[fieldName] = $(bookFieldPrefix + fieldName).val();
    });
    return book;
}

function checkBookCreatorDialogFields() {
    var fieldNamePrefix = "#new-book-";
    var isTextFieldsValid = checkDialogFields(
        fieldNamePrefix, ["isbn", "name", "authors", "volume-name"], function(fieldValue) {
            return Boolean(fieldValue);
        }
    );
    var isNumericFieldsValid = checkDialogFields(
        fieldNamePrefix, ["year", "pages", "price", "publication", "volume-number"], function(fieldValue) {
            var isNumericValid = isFinite(fieldValue);
            return (isNumericValid) ? fieldValue > 0 : false;
        }
    );
    return isTextFieldsValid && isNumericFieldsValid;
}

function checkDialogFields(fieldNamePrefix, fields, checkValue) {
    var isDialogFieldsValid = true;
    fields.forEach(function(fieldName) {
        var fieldElement = $(fieldNamePrefix + fieldName);
        var fieldValue = fieldElement.val();
        isDialogFieldsValid = checkValue(fieldValue);
        fieldElement.parent().toggleClass("has-danger", !isDialogFieldsValid);
    });
    return isDialogFieldsValid;
}
