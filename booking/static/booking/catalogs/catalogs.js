$(function() {
    receiveCatalogsItems();
});

function receiveCatalogsItems() {
    invokeCatalogsServices("info", {}, catalogsTableBuilder);
}

function catalogsTableBuilder(response) {
    clearCatalogsTable();
    if (response.success) {
        $("#catalogs-table").find("tbody").append(response.objects.reduce(function(markup, catalog) {
            columnTemplate = "<td class=\"catalog-%s\">%s</td>";
            var catalogProvider = sprintf(columnTemplate, "provider", catalog.provider.name);
            var catalogItems = sprintf(columnTemplate, "items", catalog.items);
            var catalogRefreshDate = sprintf(columnTemplate, "refresh-date", catalog.refresh_date);
            var catalogActions = createActionsTableColumnMarkup([
                {"type": "info", "img": "refresh", "func": sprintf("refreshCatalog(%d)", catalog.id), "title": "Обновить"},
            ]);
            return sprintf("%s<tr>%s%s%s%s</tr>", markup, catalogProvider, catalogItems, catalogRefreshDate, catalogActions);
        }, ""));
    } else {
        alert(sprintf("Error: %s.", response.message));
    }
}

function refreshCatalog(catalogId) {
    $("#catalog-refresh-dialog").modal("show");
    invokeCatalogsServices("refresh", {"catalog_id": catalogId}, function(response) {
        if (!response.success) {
            alert(sprintf("Error: %s", response.message));
        }
        receiveCatalogsItems();
        $("#catalog-refresh-dialog").modal("hide");
    });
}

function clearCatalogsTable() {
    clearTable("catalogs-table");
}
