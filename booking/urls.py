from django.contrib.auth import views as auth_views
from django.conf.urls import url
from . import views

app_name = "booking"
urlpatterns = [
    url(r"^$", views.booking_main_page, name = "main_page"),
    url(r"^orders/$", views.booking_orders_page, name = "orders_page"),
    url(r"^catalogs/$", views.booking_catalogs_page, name = "catalogs_page"),
    url(r"^orders/editor/$", views.booking_order_editor, name = "order_editor"),
    url(r"^books/services/$", views.booking_books_services, name = "books_services"),
    url(r"^orders/services/$", views.booking_orders_services, name = "orders_services"),
    url(r"^catalogs/services/$", views.booking_catalogs_services, name = "catalogs_services"),
    url(r"^login/$", auth_views.login, {"template_name": "booking/login.html"}, name = "login_page"),
    url(r"^logout/$", views.booking_logout, name = "logout_page"),
]
