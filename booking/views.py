from django.contrib import auth
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from .models import Provider
from .models import ORDER_STATES

from .services import BooksServices
from .services import OrdersServices
from .services import CatalogsServices

ORDER_STATES = list(enumerate(ORDER_STATES, 1))

@login_required
def booking_logout(request):
    auth.logout(request)
    return redirect("/zakaz/login")

@login_required
def booking_main_page(request):
    user = request.user
    providers = Provider.objects.all()
    return render(request, "booking/main.html", context = {
        "user": user,
        "states": ORDER_STATES,
        "providers": providers,
    })

@login_required
def booking_orders_page(request):
    user = request.user
    providers = Provider.objects.all()
    return render(request, "booking/orders.html", context = {
        "user": user,
        "states": ORDER_STATES,
        "providers": providers
    })

@login_required
def booking_catalogs_page(request):
    user = request.user
    providers = Provider.objects.all()
    return render(request, "booking/catalogs.html", context = {
        "user": user,
        "providers": providers,
    })

@login_required
def booking_order_editor(request):
    user = request.user
    providers = Provider.objects.all()
    order_id = int(request.GET.get("order_id", 0))
    return render(request, "booking/order_editor.html", context = {
        "user": user,
        "states": ORDER_STATES,
        "order_id": order_id,
        "providers": providers
    })

@login_required
def booking_books_services(request):
    return BooksServices(request).exec()

@login_required
def booking_orders_services(request):
    return OrdersServices(request).exec()

@login_required
def booking_catalogs_services(request):
    return CatalogsServices(request).exec()
