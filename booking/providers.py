import requests
import lxml.html

from .models import Provider
from .models import Book

class Curl:
    def sendGetRequest(self, url):
        response = ""
        try:
            response = requests.get(url).content
        except requests.RequestException:
            pass
        return response

class CatalogItem:
    def __init__(self, url, book):
        self._url = url
        self._book = book
        
    @property
    def url(self):
        return self._url
    
    @property
    def book(self):
        return self._book
    
    def __hash__(self):
        return hash(self._book)
    
    def __eq__(self, o):
        return hash(self) == hash(o)
    
class UnableExtractData(Exception): pass

class MashinCatalogManager:
    def __init__(self):
        self._url = "http://www.mashin.ru"
        self._url_catalog = "/?show_all=0"
        self._provider = Provider.objects.get(name = "Машиностроение")
        
    @property
    def provider(self):
        return self._provider
        
    def receive_items(self):
        catalog = Curl().sendGetRequest(self._url + self._url_catalog)
        return self.parse_catalog(catalog) if catalog else []
        
    def parse_catalog(self, catalog):
        catalog_elements = self.extract_catalog_elements(catalog)
        for element in catalog_elements:
            try:
                element_url = self._url + self.extract_element_url(element)
                element_title = self.extract_element_title(element)
                yield CatalogItem(element_url, Book(
                    name = element_title, provider = self._provider
                ))
            except UnableExtractData:
                continue
            
    def extract_catalog_elements(self, page):
        catalog_tree = lxml.html.fromstring(page)
        return catalog_tree.get_element_by_id("books").xpath("div/div[2]/ul/li")
    
    def extract_element_url(self, element):
        element_url = element.xpath("div[1]/a/@href")
        if element_url: return element_url.pop()
        else: raise UnableExtractData()
    
    def extract_element_title(self, element):
        element_title = element.xpath("div[2]/@title")
        if element_title: return element_title.pop()
        else: raise UnableExtractData()
        
    def complete_item(self, catalog_item):
        catalog_page = Curl().sendGetRequest(catalog_item.url)
        catalog_page_tree = self.extract_catalog_item_page_tree(catalog_page)
        self.parse_catalog_page_tree(catalog_page_tree, catalog_item)
        return catalog_item
        
    def parse_catalog_page_tree(self, catalog_page_tree, catalog_item):
        try:
            catalog_item.book.price = self.extract_price(catalog_page_tree)
            for param, elem in self.extract_catalog_rows(catalog_page_tree):
                if (
                    param == "Авторы" or
                    param == "Под редакцией" or
                    param == "Под общей редакцией"
                ):
                    catalog_item.book.authors = ", ".join(i for i in elem.xpath("a/text()"))
                elif param == "Год издания":
                    catalog_item.book.year = int(elem.text)
                elif param == "Объем (стр.)":
                    catalog_item.book.pages = int(elem.text)
                elif param == "ISBN":
                    catalog_item.book.isbn = elem.text
        except UnableExtractData:
            pass
    
    def extract_price(self, catalog_page_tree):
        try:
            price = catalog_page_tree.find_class("price").pop()
            price = price.xpath("big/text()").pop()
            return int(price)
        except IndexError:
            raise UnableExtractData
    
    def extract_catalog_rows(self, catalog_page_tree):
        try:
            rows = catalog_page_tree.find_class("styled_table").pop()
            rows = rows.xpath("table/tr")
            return tuple((
                row.xpath("td[1]/p/span/text()").pop(),
                row.xpath("td[2]").pop()
            ) for row in rows)
        except IndexError:
            raise UnableExtractData()
        
    def extract_catalog_item_page_tree(self, catalog_page):
        catalog_page_tree = lxml.html.fromstring(catalog_page)
        return catalog_page_tree.get_element_by_id("book")
        
