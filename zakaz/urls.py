from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^zakaz/', include('booking.urls')),
    url(r'^admin/', admin.site.urls),
]
